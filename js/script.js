const toggleBtn = document.querySelector('.navbar-toggle');
const navbarLinks = document.querySelectorAll('.navbar-link');

toggleBtn.addEventListener('click', () =>{
   navbarLinks.forEach(link => {
   	link.classList.toggle('active');
   });
});

const header = document.querySelector('header');

window.addEventListener('scroll', ()=>{
	header.classList.toggle('sticky-top', window.scrollY > 0);
});



const favorites = [{
	id: 1,
	title: 'Alfredo Pasta',
	price: 'Php480 - Php575',
	img: 'featured1.png'
},{
	id: 2,
	title: 'Baby Back Ribs',
	price: 'Php725 - Php1,400',
	img: 'featured2.png'
},{
	id: 3,
	title: 'Beef Salpicao',
	price: 'Php785',
	img: 'featured3.png'
},{
	id: 4,
	title: 'Molten Chocolate Cake',
	price: 'Php395',
	img: 'featured4.png'
}];

const card = (data) =>{
	let path = './assets/img/';

	return `
		<div class="card">
			<div class="img-wrapper">
				<img src="${path}${data && data.img ? data.img: 'featured1.png'}">
			</div>
			<div class="content">
				<h3 class="title">${data && data.title ? data.title: 'Alfredo Pasta'}</h3>
				<p class="price">${data && data.price ? data.price: 'Free'}</p>
			</div>
		</div>
	`
}

(()=>{
	const favContainer = document.querySelector('.favorites-content');
	let html = '';

	favorites.forEach(item => {
		html += card(item);
	});

	favContainer.innerHTML = html;

})();